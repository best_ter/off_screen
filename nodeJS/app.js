const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const socketIo = require('socket.io');
const responseTime = require('response-time');
const status = require('express-status-monitor');

const {performance, PerformanceObserver} = require('perf_hooks');

const app = express();
const server = app.listen(8080);
const io = socketIo.listen(server);

app.use(express.static( "public" ));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(responseTime(function(req, res, time){
    console.log(req.method, req.url, time, 'ms');
}));
app.use(status({websocket: io}));

app.set('view engine', 'ejs');

app.get('/', function(req, res){
    res.render('index', {yaw: 0, pitch: 0, radius: 3, resChooser: '{"width":960, "height":540}', meshChooser: "suzanne"});
});

// Fonction d'observation des performances
const obs = new PerformanceObserver((items) => {
    console.log(items.getEntries()[0].name+ " : " +items.getEntries()[0].duration+'ms');
    performance.clearMarks();
  });
  obs.observe({ entryTypes: ['measure'] });

// Fonction permettant d'émettre une requête au serveur de rendu et de retourner
// un résultat au client web
function requestAndAnswer(socket, options) {

    // Requête http vers le serveur de rendu
    const req = http.request(options, function(res)
    {
        let chunks = [];
        
        res.on('data', (chunk) => {
            chunks.push(Buffer.from(chunk));
        });

        res.on('end', () => {
            let result = Buffer.concat(chunks).toString('base64');
            socket.emit('imageEvent', result);
        });

    });
    
    req.on('error', error => {
        console.error(error)
    });
    
    req.end();
}

// Gestion de la connexion du client (navigateur)
io.sockets.on('connection' , function(socket)
{
    // Évènement : connexion établie
    socket.on('connectionEtablished', function(mesh, res, yaw, pitch, radius)
    {
        socket.emit('wait');
        // Chemin du nouveau mesh à charger
        let meshPath = "mesh/" + mesh + ".obj"; 
        let r = JSON.parse(res);

        let options = {
            hostname: 'localhost',
            port: 8081,
            path: '/render/mesh=(' + meshPath + ')/width=(' + r.width + ')/height=(' + r.height + ')/yaw=(' + yaw + ')/pitch=(' + pitch + ')/radius=(' + radius + ')',
            method: 'GET'
        }

        requestAndAnswer(socket, options);
    });

    // Évènement : changement de valeur sur les curseurs
    socket.on('sliderChanged', function(yaw, pitch, radius)
    {
        let options = {
            hostname: 'localhost',
            port: 8081,
            path: '/render/yaw=(' + yaw + ')/pitch=(' + pitch + ')/radius=(' + radius + ')',
            method: 'GET'
        }

        requestAndAnswer(socket, options);
    });

    // Évènement : changement du mesh ou de la résolution
    socket.on('meshChanged', function(mesh)
    {
        socket.emit('wait');

        // Chemin du nouveau mesh à charger
        let newMeshPath = "mesh/" + mesh + ".obj";

        let options = {
            hostname: 'localhost',
            port: 8081,
            path: '/render/mesh=(' + newMeshPath + ')',
            method: 'GET'
        }

        requestAndAnswer(socket, options);
    });

    // Évènement : changement de la résolution
    socket.on('resChanged', function(res)
    {
        socket.emit('wait');

        // Nouvelle résolution
        let r = JSON.parse(res);

        let options = {
            hostname: 'localhost',
            port: 8081,
            path: '/render/width=(' + r.width + ')/height=(' + r.height + ')',
            method: 'GET'
        }

        requestAndAnswer(socket, options);
    });
});