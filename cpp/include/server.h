#ifndef SERVER_H
#define SERVER_H

#include <cppcms/application.h>
#include <cppcms/url_dispatcher.h>  
#include <cppcms/url_mapper.h> 

#include <imageDelivery.h>
/**
 * Serveur http simple permettant de communiquer avec l'interface NodeJS via des url.
 * Appelle les fonctions du service de création d'image avec les paramètres spécifiés dans les url.
 */
class Server: public cppcms::application
{
public:
    /**
     * Server
     * 
     * @param  {cppcms::service} srv : service créé lors du lancement du programme (main)
     * 
     */
    Server(cppcms::service &srv);
    /**
     * ~Server
     * 
     * Détruit le service de création d'image associé.
     */
    ~Server();

private:
    /**
     * Demande au service de création d'image d'initialiser le contexte OpenGL courant et d'effectuer un premier rendu de l'objet 3D spécifié.
     * 
     * @param  {std::string} mesh   : maillage à charger
     * @param  {std::string} width  : largeur de la fenêtre
     * @param  {std::string} height : hauteur de la fenêtre
     * @param  {std::string} yaw    : position de la caméra sur le cercle horizontal de rayon radius autour du centre (en degrés)
     * @param  {std::string} pitch  : position de la caméra sur le cercle vertical de rayon radius autour du centre (en degrés)
     * @param  {std::string} radius : distance de la caméra par rapport au centre
     * 
     * L'initialisation créé la fenêtre de rendu (cachée) ainsi que le contexte OpenGL associé.
     * La taille de la fenêtre correspond à la résolution de l'image produite.
     * On charge ensuite l'objet 3D, on place la caméra et on effectue le rendu.
     */
    virtual void initAndRender(std::string mesh, std::string width, std::string height, std::string yaw, std::string pitch, std::string radius);
    /**
     * Demande au service de création d'image de modifier la position de la caméra et de procéder à un nouveau rendu de l'objet 3D.
     * 
     * @param  {std::string} yaw    : position de la caméra sur le cercle horizontal de rayon radius autour du centre (en degrés)
     * @param  {std::string} pitch  : position de la caméra sur le cercle vertical de rayon radius autour du centre (en degrés)
     * @param  {std::string} radius : distance de la caméra par rapport au centre
     */
    virtual void changeCamAndRender(std::string yaw, std::string pitch, std::string radius);
    /**
     * Demande au service de création d'image de canger l'objet 3D à rendre et effectue un nouveau rendu.
     * 
     * @param  {std::string} mesh : maillage à charger
     */
    virtual void changeMeshAndRender(std::string mesh);
    /**
     * Demande au service de création d'image de changer la résolution du rendu et effectue un nouveau rendu.
     * 
     * @param  {std::string} width  : largeur de la fenêtre
     * @param  {std::string} height : hauteur de la fenêtre
     */
    virtual void changeResAndRender(std::string width, std::string height);

private:
    ImageDeliveryService m_ids;
};

#endif