#ifndef SHADER_H
#define SHADER_H

#include <glad/glad.h>
#include <glm/glm.hpp>

#include <string>
#include <cstring>
#include <fstream>
#include <sstream>
#include <iostream>


class Shader
{
public:
    /**
     * Shader 
     * 
     * Constructeur par défaut.
     */
    Shader();
    /**
     * Crée le programme shader à partir du vertex shader et du fragment shader donnés en paramètre.
     * 
     * @param  {char*} vertexPath   : chemin vers le vertex shader
     * @param  {char*} fragmentPath : chemin vers le fragment shader
     */
    void init(const char* vertexPath, const char* fragmentPath);
    /**
     * Associe le programme shader au contexte OpenGL courant.
     */
    void use();
    /**
     * Supprime le programme shader du contexte courant
     */
    void destroy();
    /**
     * Spécifie la valeur d'une variable uniforme de type vec3 dans le programme shader
     * 
     * @param  {std::string} name : nom de la variable uniforme dans le programme shader
     * @param  {glm::vec3} vec    : variable à associer
     */
    void setVec3(const std::string &name, const glm::vec3 &vec) const;
    /**
     * Spécifie la valeur d'une variable uniforme de type mat4 dans le programme shader
     * 
     * @param  {std::string} name : nom de la variable uniforme dans le programme shader
     * @param  {glm::mat4} mat    : variable à associer
     */
    void setMat4(const std::string &name, const glm::mat4 &mat) const;
    /**
     * Retourne l'identificateur du programme shader
     * 
     * @return {unsigned}  : identificateur du programme shader
     */
    unsigned int getID();

private:
    unsigned int m_ID;
};

#endif // SHADER_H
