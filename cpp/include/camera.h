#ifndef CAMERA_H
#define CAMERA_H

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <vector>

const glm::vec3 TARGET = glm::vec3(0.0, 0.0, 0.0);
const glm::vec3 UP = glm::vec3(0.0, 1.0, 0.0);
/**
 * La classe caméra permet de représenter la position du regard de l'utilisateur par rapport à l'objet 3D (sa direction étant fixée au centre de la scène).
 */
class Camera
{
public:
    /**
     * Camera
     * 
     * @param  {float} yaw    : angle de rotation horizontal autour de la cible.
     * @param  {float} pitch  : angle de rotation vertical autour de la cible.
     * @param  {float} radius : distance à la cible.
     * 
     * La caméra est définie selon 3 critères : sa position, la position de la cible et le vecteur up ("haut" de la caméra).
     * Ici la cible est toujours en (0, 0, 0) et le vecteur up est toujours égal à (0, 1, 0). La position est calculée en fonction des paramères.
     */
    Camera(float yaw, float pitch, float radius);

    /**
     * Bouge la caméra selon les nouvelles valeurs des paramètres yaw, pitch et radius.
     * 
     * @param  {float} yaw    : angle de rotation horizontal autour de la cible.
     * @param  {float} pitch  : angle de rotation vertical autour de la cible.
     * @param  {float} radius : distance à la cible.
     */
    void move(float yaw, float pitch, float radius);

    /**
     * Retourne la matrice view correspondant à la caméra.
     * 
     * @return {glm::mat4}  : matrice view correspondant à la caméra.
     * 
     * La matrice view dépend de la position de la caméra, de la direction du regard et du vecteur désignant le "haut" de la caméra.
     * Ces valeurs sont définies lors de la construction de l'objet.
     */
    glm::mat4 getViewMatrix();

private:
    glm::vec3 m_position;
    glm::vec3 m_target;
    glm::vec3 m_up;
};

#endif //CAMERA_H