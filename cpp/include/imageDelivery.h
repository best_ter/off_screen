#ifndef IMAGE_DELIVERY_H
#define IMAGE_DELIVERY_H

#include <shader.h>
#include <mesh.h>

class GLFWwindow;
class Camera;
/**
 * La classe ImageDeliveryService s'occupe du contexte OpenGL et de la fenêtre de rendu (cachée) permettant d'effectuer le rendu (off-screen) d'un objet 3D chargé.
 * Elle s'occupe de délivrer une image correspondant au rendu effectué, en fonction de la position de la caméra courante et de la résolution choisie.
 */
class ImageDeliveryService
{
public:
    /**
     * ImageDeliveryService
     * 
     * Constructeur par défaut.
     */
    ImageDeliveryService();
    /**
     * Initialise le contexte OpenGL et les shaders.
     * 
     * @param  {int} width  : Largeur de la fenêtre de rendu
     * @param  {int} height : Hauteur de la fenêtre de rendu
     * 
     * Cette fonction permet d'initialiser le contexte OpenGL ainsi que les shaders afin de pouvoir ensuite faire le rendu de scènes 3D.
     * Plus précisément, on initialise GLFW (qui va s'occuper du contexte et de la fenêtre de rendu) et GLAD (loading library).
     * La taille de la fenêtre de rendu (et par extension du viewport, ainsi que la résolution de l'image résultante) sont passés en paramètre du constructeur.
     * La création du programme shader est également effectuée ici.
     */
    void init(int width, int height);
    /**
     * Supprime le contexte OpenGL courant ainsi que le programme shader.
     */
    void release();
    /**
     * Charge un maillage afin qu'il soit utilisable par OpenGL (et la carte graphique).
     * 
     * @param  {std::string} path : chemin vers le maillage en question.
     * 
     * Le maillage doit être présent au format obj. De plus, il faut s'assurer que le contexte OpenGL soit bien initialisé car on va stocker
     * les données du maillage (points, normales ...) dans une VAO et des VBOs.
     */
    void loadMesh(const std::string &path);
    /**
     * Délivre l'image du rendu de l'objet 3D couramment chargé.
     * 
     * @param  {std::ostream} out : flux de sortie dans lequel retourner l'image.
     * 
     * L'image est retournée au format JPEG, et à la résolution courante définie par l'utilisateur.
     */
    void deliver(std::ostream& out);
    /**
     * Modifie la position de la caméra en fonction des paramètres donnés.
     * 
     * @param  {double} yaw    : angle horizontal
     * @param  {double} pitch  : angle vertical
     * @param  {double} radius : rayon
     * 
     * L'angle horizontal et vertical sont par rapport au centre (0, 0, 0), la caméra pointant toujours dans cette direction.
     * Le rayon correspond à la distance entre la caméra et le centre.
     */
    void moveCam(double yaw, double pitch, double radius);
    /**
     * Change la taille de la fenêtre de rendu (cachée).
     * 
     * @param  {int} width  : Largeur de la fenêtre (zone d'affichage)
     * @param  {int} height : Hauteur de la fenêtre (zone d'affichage)
     * 
     * Modifie également la taille du viewport associé à la fenêtre et recrée un nouveau FBO.
     */
    void setWindow(int width, int height);

private:
    bool m_isInit = false;
    float m_width, m_height;
    GLuint m_fbo;
    GLuint m_render_buf[2];
    GLFWwindow *m_window;
    Camera *m_cam;
    Shader m_shader;
    Mesh m_mesh;
};

#endif // IMAGE_DELIVERY_H