#ifndef MESH_H
#define MESH_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <string>
/**
 * La classe mesh permet de charger un maillage dans les structures de données OpenGL et d'effectuer le rendu de l'objet par la carte graphique.
 */
class Mesh
{
public:
    /**
     * Mesh
     * 
     * Constructeur par défaut.
     */
    Mesh();
    /**
     * Parcourt le maillage et le stocke dans les structures de données OpenGL adéquates.
     * 
     * @param  {std::string} path : chemin vers le maillage en question.
     * 
     * Le maillage doit être présent au format obj.
     * Charge ses données pour qu'elles soient utilisables par le GPU (utilisation d'un VAO et de VBOs).
     * 
     * S'assurer que le contexte OpenGL est initialisé et courant.
     */
    void load(const std::string &path);
    /**
     * Effectue le rendu du maillage dans le framebuffer du contexte courant.
     * 
     * S'assurer que le contexte OpenGL est initialisé et courant.
     */
    void draw();
    /**
     * Vide les buffers OpenGL contenant les données de l'objet 3D courant.
     * 
     * S'assurer que le contexte OpenGL est initialisé et courant.
     */
    void destroy();

private:
    unsigned int m_vao;
    unsigned int m_vbos[3];
    size_t m_size;
};

#endif //MESH_H
