#
# Try to find FREEIMAGE
# Once done this will define
#  
# FREEIMAGE_FOUND           - system has FREEIMAGE
# FREEIMAGE_INCLUDE_DIRS    - the FREEIMAGE include directories
# FREEIMAGE_LIBRARIES       - Link these to use FREEIMAGE
# FREEIMAGE_LIBRARY_DIR     - directory where the libraries are included

cmake_minimum_required(VERSION 2.8.9)

#if already found via finder or simulated finder in openmesh CMakeLists.txt, skip the search
IF (NOT FREEIMAGE_FOUND) 
  SET (SEARCH_PATHS 
    /usr/local/
    /usr/
    "${FREEIMAGE_LIBRARY_DIR}"
  )

  FIND_PATH (FREEIMAGE_INCLUDE_DIR FreeImage.h
    PATHS ${SEARCH_PATHS}
    PATH_SUFFIXES include)

  FIND_LIBRARY(FREEIMAGE_LIBRARY NAMES freeimage
    PATHS ${SEARCH_PATHS}
    PATH_SUFFIXES lib lib64)

#checks, if FreeImage was found and sets FREEIMAGE_FOUND if so
  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(FreeImage DEFAULT_MSG FREEIMAGE_INCLUDE_DIR FREEIMAGE_LIBRARY)
 
#sets the library dir 
  if ( FREEIMAGE_LIBRARY )
    get_filename_component(_FREEIMAGE_LIBRARY_DIR ${FREEIMAGE_LIBRARY} PATH)
  endif( FREEIMAGE_LIBRARY )
  set (FREEIMAGE_LIBRARY_DIR "${_FREEIMAGE_LIBRARY_DIR}" CACHE PATH "The directory where the FreeImage libraries can be found.")
  
 
  mark_as_advanced(FREEIMAGE_INCLUDE_DIR FREEIMAGE_LIBRARY FREEIMAGE_LIBRARY_DIR)
endif()
