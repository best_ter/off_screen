#
# Try to find CPPCMS
# Once done this will define
#  
# CPPCMS_FOUND           - system has CPPCMS
# CPPCMS_INCLUDE_DIR    - the CPPCMS include directories
# CPPCMS_LIBRARIES       - Link these to use CPPCMS
# CPPCMS_LIBRARY_DIR     - directory where the libraries are included

cmake_minimum_required(VERSION 2.8.9)

#if already found via finder or simulated finder in cppms CMakeLists.txt, skip the search
IF (NOT CPPCMS_FOUND) 
  SET (SEARCH_PATHS 
    /usr/local/
    /usr/
    "${CPPCMS_LIBRARY_DIR}"
  )

  FIND_PATH (CPPCMS_INCLUDE_DIR config.h
    PATHS ${SEARCH_PATHS}
    PATH_SUFFIXES include/cppcms)

  FIND_LIBRARY(CPPCMS_LIBRARY NAMES cppcms
    PATHS ${SEARCH_PATHS}
    PATH_SUFFIXES lib lib64)

  FIND_LIBRARY(BOOSTER_LIBRARY NAMES booster
    PATHS ${SEARCH_PATHS}
    PATH_SUFFIXES lib lib64)

  set(CPPCMS_LIBRARIES ${CPPCMS_LIBRARY} ${BOOSTER_LIBRARY})

#checks, if CppCMS was found and sets CPPCMS_FOUND if so
  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(CppCMS DEFAULT_MSG CPPCMS_INCLUDE_DIR CPPCMS_LIBRARY)
 
#sets the library dir 
  if ( CPPCMS_LIBRARY )
    get_filename_component(_CPPCMS_LIBRARY_DIR ${CPPCMS_LIBRARY} PATH)
  endif( CPPCMS_LIBRARY )
  set (CPPCMS_LIBRARY_DIR "${_CPPCMS_LIBRARY_DIR}" CACHE PATH "The directory where the CppCMS libraries can be found.")
  
 
  mark_as_advanced(CPPCMS_INCLUDE_DIR CPPCMS_LIBRARY CPPCMS_LIBRARY_DIR)
endif()