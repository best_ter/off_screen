#
# Try to find FREEIMAGEPLUS
# Once done this will define
#  
# FREEIMAGEPLUS_FOUND           - system has FREEIMAGEPLUS
# FREEIMAGEPLUS_INCLUDE_DIRS    - the FREEIMAGEPLUS include directories
# FREEIMAGEPLUS_LIBRARIES       - Link these to use FREEIMAGEPLUS
# FREEIMAGEPLUS_LIBRARY_DIR     - directory where the libraries are included

cmake_minimum_required(VERSION 2.8.9)

#if already found via finder or simulated finder in openmesh CMakeLists.txt, skip the search
IF (NOT FREEIMAGEPLUS_FOUND) 
  SET (SEARCH_PATHS 
    /usr/local/
    /usr/
    "${FREEIMAGEPLUS_LIBRARY_DIR}"
  )

  FIND_PATH (FREEIMAGEPLUS_INCLUDE_DIR FreeImagePlus.h
    PATHS ${SEARCH_PATHS}
    PATH_SUFFIXES include)

  FIND_LIBRARY(FREEIMAGEPLUS_LIBRARY NAMES freeimageplus
    PATHS ${SEARCH_PATHS}
    PATH_SUFFIXES lib lib64)

#checks, if FreeImagePlus was found and sets FREEIMAGEPLUS_FOUND if so
  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(FreeImagePlus DEFAULT_MSG FREEIMAGEPLUS_INCLUDE_DIR FREEIMAGEPLUS_LIBRARY)
 
#sets the library dir 
  if ( FREEIMAGEPLUS_LIBRARY )
    get_filename_component(_FREEIMAGEPLUS_LIBRARY_DIR ${FREEIMAGEPLUS_LIBRARY} PATH)
  endif( FREEIMAGEPLUS_LIBRARY )
  set (FREEIMAGEPLUS_LIBRARY_DIR "${_FREEIMAGEPLUS_LIBRARY_DIR}" CACHE PATH "The directory where the FreeImagePlus libraries can be found.")
  
 
  mark_as_advanced(FREEIMAGEPLUS_INCLUDE_DIR FREEIMAGEPLUS_LIBRARY FREEIMAGEPLUS_LIBRARY_DIR)
endif()
