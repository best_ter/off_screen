{  
    "service" : {  
        "api" : "http",  
        "port" : 8081,
        "ip" : "0.0.0.0",
        "worker_threads" : 1
    },  
    "http" : {  
        "script_names" : [ "/offscreen" ]  
    }  
}
