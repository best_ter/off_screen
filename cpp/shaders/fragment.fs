#version 330 core

uniform vec3 lightPos;
uniform vec3 lightColor;

in vec3 color;
in vec3 normal;
in vec3 fragPos;

void main()
{
    // Lumirère ambiente
    float ambientStrength = 0.1;
    vec3 ambient = ambientStrength * lightColor;

    // Lumière diffuse
    vec3 norm = normalize(normal);
    vec3 lightDir = normalize(lightPos - fragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;

    // Résultat
    vec3 result = (ambient + diffuse) * color;
    gl_FragColor = vec4(result, 1.0);
}
