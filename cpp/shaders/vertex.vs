#version 330 core

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aCol;
layout (location = 2) in vec3 aNor;

out vec3 fragPos;
out vec3 color;
out vec3 normal;

void main()
{
    fragPos = vec3(model * vec4(aPos, 1.0));
    color = aCol;
    normal = mat3(transpose(inverse(model))) * aNor;

    gl_Position = projection * view * model * vec4(aPos, 1.0f);
}
