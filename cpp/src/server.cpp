#include <cppcms/http_response.h>

#include <fstream>

#include <server.h>

Server::Server(cppcms::service &srv): 
cppcms::application(srv)
{
    // Initialisation + rendu (première connexion)
    dispatcher().assign("/render/mesh=\\((.*)\\)/width=\\((.*)\\)/height=\\((.*)\\)/yaw=\\((.*)\\)/pitch=\\((.*)\\)/radius=\\((.*)\\)", &Server::initAndRender, this, 1, 2, 3, 4, 5, 6);
    // Changement des valeurs de caméra
    dispatcher().assign("/render/yaw=\\((.*)\\)/pitch=\\((.*)\\)/radius=\\((.*)\\)", &Server::changeCamAndRender, this, 1, 2, 3);
    // Changement de mesh
    dispatcher().assign("/render/mesh=\\((.*)\\)", &Server::changeMeshAndRender, this, 1);
    // Chargement de résolution
    dispatcher().assign("/render/width=\\((.*)\\)/height=\\((.*)\\)", &Server::changeResAndRender, this, 1, 2);
}

Server::~Server()
{
    m_ids.release();
}

// Initialisation + rendu (première connexion)
void Server::initAndRender(std::string mesh, std::string width, std::string height, std::string yaw, std::string pitch, std::string radius)
{
    m_ids.init(atoi(width.c_str()), atoi(height.c_str()));
    m_ids.loadMesh(mesh);
    m_ids.moveCam(atof(yaw.c_str()), atof(pitch.c_str()), atof(radius.c_str()));
    m_ids.deliver(response().out());
}

// Changement des valeurs de caméra
void Server::changeCamAndRender(std::string yaw, std::string pitch, std::string radius)
{
    m_ids.moveCam(atof(yaw.c_str()), atof(pitch.c_str()), atof(radius.c_str()));
    m_ids.deliver(response().out());
}

// Changement de mesh
void Server::changeMeshAndRender(std::string mesh)
{
    m_ids.loadMesh(mesh);
    m_ids.deliver(response().out());
}

// Chargement de résolution
void Server::changeResAndRender(std::string width, std::string height)
{
    m_ids.setWindow(atoi(width.c_str()), atoi(height.c_str()));
    m_ids.deliver(response().out());
}