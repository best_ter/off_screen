#include <camera.h>
#include <iostream>

Camera::Camera(float yaw, float pitch, float radius)
{
    move(yaw, pitch, radius);
    m_target = TARGET;
    m_up = UP;
}

void Camera::move(float yaw, float pitch, float radius)
{
    auto posX = radius * cos(glm::radians(yaw + 90)) * cos(glm::radians(pitch));
    auto posY = radius * sin(glm::radians(pitch));
    auto posZ = radius * sin(glm::radians(yaw + 90)) * cos(glm::radians(pitch));

    m_position = glm::vec3(posX, posY, posZ);
}

glm::mat4 Camera::getViewMatrix()
{
    return glm::lookAt(m_position, m_target, m_up);
}