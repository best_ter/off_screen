#include <shader.h>

Shader::Shader()
{
    m_ID = 0;
}

void Shader::init(const char* vertexPath, const char* fragmentPath) {

    std::string vertexCode, fragmentCode;
    std::ifstream vShaderFile, fShaderFile;

    // On s'assure que les ifstream peuvent retourner ces exceptions
    vShaderFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);
    fShaderFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);

    try
    {
        // Ouverture
        vShaderFile.open(vertexPath);
        fShaderFile.open(fragmentPath);
        std::stringstream vShaderStream, fShaderStream;
        // Lecture
        vShaderStream << vShaderFile.rdbuf();
        fShaderStream << fShaderFile.rdbuf();
        // Fermeture
        vShaderFile.close();
        fShaderFile.close();
        // Conversion stream -> string
        vertexCode = vShaderStream.str();
        fragmentCode = fShaderStream.str();
    }
    catch(std::ifstream::failure &e)
    {
        std::cout << "Error : shader file not succesfully read" << std::endl;
    }

    // Conversion string -> char*
    const char* vShaderCode = vertexCode.c_str();
    const char* fShaderCode = fragmentCode.c_str();

    unsigned int vertexShader, fragmentShader;
    int  success;
    char infoLog[512];

    // Crée le vertex shader, fournit le code source, le compile et on vérifie que ça a marché
    vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vShaderCode, NULL);
    glCompileShader(vertexShader);
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);

    if(!success)
    {
        glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
        std::cout << "Error : vertex shader compilation failed\n" << infoLog << std::endl;
    }

    memset(infoLog, 0, sizeof(infoLog));

    // Crée le fragment shader, fournit le code source, le compile et on vérifie que ça a marché
    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fShaderCode, NULL);
    glCompileShader(fragmentShader);

    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
    if(!success)
    {
        glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
        std::cout << "Error : fragment shader compilation failed\n" << infoLog << std::endl;
    }

    memset(infoLog, 0, sizeof(infoLog));

    // Crée le shader program, ajoute les shaders, link puis vérifie que ça a marché
    m_ID = glCreateProgram();
    glAttachShader(m_ID, vertexShader);
    glAttachShader(m_ID, fragmentShader);
    glLinkProgram(m_ID);

    glGetProgramiv(m_ID, GL_LINK_STATUS, &success);
    if(!success)
    {
        glGetProgramInfoLog(m_ID, 512, NULL, infoLog);
        std::cout << "Error : shader program compilation failed\n" << infoLog << std::endl;
    }

    // On supprime les shaders (plus besoin)
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
}

void Shader::use()
{
    glUseProgram(m_ID);
}

void Shader::destroy()
{
    glDeleteProgram(m_ID);
}

void Shader::setVec3(const std::string &name, const glm::vec3 &vec) const
{
    glUniform3f(glGetUniformLocation(m_ID, name.c_str()), vec[0], vec[1], vec[2]);
}

void Shader::setMat4(const std::string &name, const glm::mat4 &mat) const
{
    glUniformMatrix4fv(glGetUniformLocation(m_ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

unsigned int Shader::getID() {
    return m_ID;
}