#include <camera.h>
#include <imageDelivery.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <FreeImagePlus.h>

#include <math.h>
#include <cstring>
#include <vector>
#include <chrono>

double REND, REND_C, READ, READ_C, SAVE, SAVE_C, COPY, COPY_C;

void init_counters() {
    REND = REND_C = READ = READ_C = SAVE = SAVE_C = COPY = COPY_C = 0;
}

// Structure déclarée dans le fichier FreeImageIO.h, dans les sources de la bibliothèque
FI_STRUCT (FIMEMORYHEADER) {
    BOOL delete_me;
    long file_length;
    long data_length;
    void *data;
    long current_position;
};

ImageDeliveryService::ImageDeliveryService()
{}

void ImageDeliveryService::init(int width, int height)
{
    std::cout << "-------------------------INIT----------------------------"<< std::endl;
    init_counters();
    auto begin = std::chrono::high_resolution_clock::now();

    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // Masquer la fenêtre de rendu

    // Initialisation fenêtre + contexte
    m_width = width, m_height = height;
    m_window = glfwCreateWindow(m_width, m_height, "Rendering", NULL, NULL);
    if(m_window == NULL)
    {
        std::cout << "Failed to create GLFW window." << std::endl;
        glfwTerminate();
        return;
    }

    glfwMakeContextCurrent(m_window);

    // Initialisation de GLAD
    if(!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD." << std::endl;
        glfwTerminate();
        return;
    }

    setWindow(width, height);

    // Configuration de l'état global d'OpenGL
    glEnable(GL_DEPTH_TEST);

    // Chargement des shaders
    m_shader.init("shaders/vertex.vs", "shaders/fragment.fs");

    // Nouvelle caméra (par défaut)
    m_cam = new Camera(0, 0, 0);

    m_isInit = true;

    glFinish(); // Sécurité pour mesurer correctement le temps
    auto end = std::chrono::high_resolution_clock::now();
    double INI = std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count();
    std::cout << "Init time : " <<  INI << " µs." << std::endl;
}

void ImageDeliveryService::release()
{
    std::cout << "------------------------RELEASE--------------------------"<< std::endl;
    if(!m_isInit)
    {
        std::cout << "Release : image delivery service not initialized." << std::endl;
        return;
    }
    init_counters();
    auto begin = std::chrono::high_resolution_clock::now();

    glfwMakeContextCurrent(m_window);

    glDeleteFramebuffers(1, &m_fbo);
    glDeleteRenderbuffers(1, m_render_buf);

    m_shader.destroy();
    m_mesh.destroy();
    glfwTerminate();
    m_window = nullptr;
    delete m_cam;
    m_cam = nullptr;
    m_isInit = false;

    auto end = std::chrono::high_resolution_clock::now();

    double REL = std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count();
    std::cout << "Release time : " <<  REL << " µs." << std::endl;
}

void ImageDeliveryService::loadMesh(const std::string &path)
{
    std::cout << "-----------------------LOADING MESH----------------------"<< std::endl;
    if(!m_isInit)
    {
        std::cout << "Load Mesh : image delivery service not initialized." << std::endl;
        return;
    }
    init_counters();
    auto begin = std::chrono::high_resolution_clock::now();

    glfwMakeContextCurrent(m_window);
    m_mesh.load(path);
    std::cout << "Mesh : " << path << "." << std::endl;

    glFinish(); // Sécurité mesurer correctement le temps
    auto end = std::chrono::high_resolution_clock::now();
    double LOAD = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
    std::cout << "Loading mesh time: " << LOAD << " ms." << std::endl;
}

void ImageDeliveryService::deliver(std::ostream& out)
{
    std::cout << "-----------------------RENDERING-------------------------"<< std::endl;
    if(!m_isInit)
    {
        std::cout << "Deliver : image delivery service not initialized." << std::endl;
        return;
    }
    auto begin = std::chrono::high_resolution_clock::now();

    glfwMakeContextCurrent(m_window);

    // Définition des matrices
    glm::mat4 model = glm::mat4(1.0f);
    glm::mat4 view = m_cam->getViewMatrix();
    glm::mat4 projection = glm::perspective(glm::radians(45.0f), float(m_width) / float(m_height), 0.1f, 100.0f);

    // Définition des vecteurs relatifs à la lumière
    glm::vec3 lightPos = glm::vec3(0.0f, 5.0f, 5.0f);
    glm::vec3 lightColor = glm::vec3(1.0f, 1.0f, 1.0f);

    ////////////////////// RENDU /////////////////////////

    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);

    glClearColor(0.2f, 0.2f, 0.5f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_shader.use();
    m_shader.setVec3("lightPos", lightPos);
    m_shader.setVec3("lightColor", lightColor);
    m_shader.setMat4("model", model);
    m_shader.setMat4("view", view);
    m_shader.setMat4("projection", projection);

    m_mesh.draw();

    glfwPollEvents(); // Obligatoire

    glFinish(); // Sécurité mesurer correctement le temps
    auto end = std::chrono::high_resolution_clock::now();
    REND_C += 1;
    REND = REND * ((REND_C - 1)/REND_C) + std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() * (1 / REND_C);
    std::cout << "Rendering mean time : " <<  REND << " µs. [" << REND_C << "]" << std::endl;

    /////////////// LECTURE FRAMEBUFFER /////////////////

    begin = std::chrono::high_resolution_clock::now();

    // Création de l'image depuis le back buffer
    fipImage image(FIT_BITMAP, m_width, m_height, 8 * 3);

    glReadBuffer(GL_COLOR_ATTACHMENT0);
    glReadPixels(0, 0, m_width, m_height, GL_RGB, GL_UNSIGNED_BYTE, image.accessPixels());

    glFinish(); // Pour mesurer correctement le temps
    end = std::chrono::high_resolution_clock::now();
    READ_C += 1;
    READ = READ * ((READ_C - 1)/READ_C) + std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() * (1 / READ_C);
    std::cout << "Readpixels mean time : " <<  READ << " µs. [" << READ_C << "]" << std::endl;

    /////////// SAUVEGARDE FORMAT IMAGE ////////////////

    begin = std::chrono::high_resolution_clock::now();

    fipMemoryIO fmio; // Sauvegarde de l'image dans un buffer intermédiaire (fmio)
    auto saved = image.saveToMemory(FIF_JPEG, fmio); // Possibilités : FIF_JPEG, FIF_PNG, FIF_WEBP, FIF_BMP, FIF_GIF

    if(!saved) std::cout << "Failed to save image into memory." << std::endl;

    end = std::chrono::high_resolution_clock::now();
    SAVE_C += 1;
    SAVE = SAVE * ((SAVE_C - 1)/SAVE_C) + std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() * (1 / SAVE_C);
    std::cout << "Saving mean time : " <<  SAVE << " µs. [" << SAVE_C << "]" << std::endl;
    
    ////////////// COPIE DANS LE BUFFER ////////////////  

    begin = std::chrono::high_resolution_clock::now();

    // Passage du contenu du fmio dans le flux de sortie
    FIMEMORYHEADER* mem_header = (FIMEMORYHEADER*)((*fmio).data);
    out.write((char*)mem_header->data, mem_header->data_length);

    out.flush(); // Pour mesurer le temps
    end = std::chrono::high_resolution_clock::now();
    COPY_C += 1;
    COPY = COPY * ((COPY_C - 1)/COPY_C) + std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() * (1 / COPY_C);
    std::cout << "Output writing mean time : " <<  COPY << " µs. [" << COPY_C << "]" << std::endl;

    std::cout << "TOTAL MEAN TIME : " << int((REND + READ + SAVE + READ) / 1000) << "ms" << std::endl;
}

void ImageDeliveryService::setWindow(int width, int height)
{
    std::cout << "-----------------------RES CHANGE------------------------"<< std::endl;
    if(!glfwInit())
    {
        std::cout << "Init window : glfw not initialized." << std::endl;
        return;
    }
    init_counters();

    m_width = width, m_height = height;

    // Création du FBO & Render Buffers
    glGenFramebuffers(1, &m_fbo);
    glGenRenderbuffers(2, m_render_buf);

    glBindFramebuffer(GL_FRAMEBUFFER,m_fbo);

    glBindRenderbuffer(GL_RENDERBUFFER, m_render_buf[0]);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_RGB, m_width, m_height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, m_render_buf[0]);
    
    glBindRenderbuffer(GL_RENDERBUFFER, m_render_buf[1]);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, m_width, m_height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_render_buf[1]);

    glfwSetWindowSize(m_window, m_width, m_height);
    glViewport(0, 0, m_width, m_height);
}

void ImageDeliveryService::moveCam(double yaw, double pitch, double radius)
{
    m_cam->move(yaw, pitch, radius);
}