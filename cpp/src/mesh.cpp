#include "mesh.h"

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>

#include <iostream>
#include <vector>
#include <cstring>

struct MyTraits : public OpenMesh::DefaultTraits
{
    VertexAttributes(OpenMesh::Attributes::Color | OpenMesh::Attributes::Normal);
};

typedef OpenMesh::TriMesh_ArrayKernelT<MyTraits> MyMesh;

Mesh::Mesh()
{
    m_vao = 0;
    memset(m_vbos, 0, sizeof(m_vbos));
    m_size = 0;
}

void Mesh::load(const std::string &path)
{
    MyMesh mesh;
    std::vector<float> vertices;
    std::vector<float> colors;
    std::vector<float> normals;

    OpenMesh::IO::Options opt;
    opt += OpenMesh::IO::Options::VertexColor;
    opt += OpenMesh::IO::Options::VertexNormal;
    if (!OpenMesh::IO::read_mesh(mesh, path, opt))
    {
      std::cout << "Error reading mesh at : " << path << std::endl;
      exit(1);
    }

    std::cout << "Nombre de sommets / normales : " << mesh.n_vertices() << std::endl;
    std::cout << "Nombre de faces : " << mesh.n_faces() << std::endl;

    // Parcours du mesh et stockage des attributs
    for (MyMesh::FaceIter f_it=mesh.faces_begin(); f_it!=mesh.faces_end(); ++f_it)
    {
        for(MyMesh::FaceVertexCCWIter fv_it=mesh.fv_ccwiter(*f_it); fv_it.is_valid(); ++fv_it)
        {
            auto &point = mesh.point(*fv_it);
            //auto &color = mesh.color(*fv_it);
            auto &normal = mesh.normal(*fv_it);
            vertices.push_back(point[0]); vertices.push_back(point[1]); vertices.push_back(point[2]);
            //colors.push_back(color[0] / 255.f); colors.push_back(color[1] / 255.f); colors.push_back(color[2] / 255.f);
            colors.push_back(0.8f); colors.push_back(0.8f); colors.push_back(0.8f);
            normals.push_back(normal[0]); normals.push_back(normal[1]); normals.push_back(normal[2]);
        }
    }

    m_size = vertices.size() / 3;

    // Création du VAO dans le GPU + bind
    glGenVertexArrays(1, &m_vao);
    glBindVertexArray(m_vao);

    unsigned int vbos[3];

    // Création des VBO dans le GPU + bind + remplissage + link
    glGenBuffers(3, vbos);

    glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), vertices.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);
    glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(float), colors.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, vbos[2]);
    glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(float), normals.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(2);

    // Unbind
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void Mesh::draw() {
    glBindVertexArray(m_vao);
    glDrawArrays(GL_TRIANGLES, 0, m_size);
    glBindVertexArray(0);
}

void Mesh::destroy() 
{
    glDeleteVertexArrays(1, &m_vao);
    glDeleteBuffers(3, m_vbos);
}
