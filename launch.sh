#!/bin/bash
cd ./cpp/build
if [[ "$1" == "cmake" ]]
then
    echo "Exécution de CMAKE"
    cmake ../
fi
echo "Exécution de make"
make
echo "Lancement du serveur de rendu..."
./app -c config.js &
echo "Lancement de l'interface NodeJS..."
cd ../../nodeJS
node app.js