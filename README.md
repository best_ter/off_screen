**Pour lancer le projet**

1. S'assurer d'avoir toutes les dépendances nécessaires pour le serveur de rendu installées sur la machine. Se référer à la page "Versions & dépendances" pour obtenir la liste des dépendances.
2. S'assurer d'avoir toutes les dépendances relatives à nodeJS : se placer dans le répertoire "nodeJS" et installer toutes les dépendances via la npm (commande npm install <nom_du_module>). Se référer à la page "Versions & dépendances" pour obtenir la liste des dépendances.
3. Créer un dossier "mesh" dans le répertoire cpp et y placer les fichiers obj à visualiser (suzanne.obj, happyBouddha.obj, nefertiti.obj, suzanne.obj).
4. Créer un dossier vide "build", toujours dans le répertoire cpp.
5. Se placer dans le répertoire "stadia" et lancer la commande "./launch.sh cmake" afin de compiler et lancer le programme pour la première fois.
6. Pour relancer le programme ultérieurement, simplement lancer la commande "./launch.sh".